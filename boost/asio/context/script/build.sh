#!/usr/bin/env bash
set -e

BashDir=$(cd "$(dirname $BASH_SOURCE)" && pwd)
eval $(cat "$BashDir/conf.sh")
if [[ "$Command" == "" ]];then
    Command="$0"
fi

function help(){
    echo "build helper"
    echo
    echo "Usage:"
    echo "  $Command [flags]"
    echo
    echo "Flags:"
    echo "  -c, --clear         clear cmake cache"
    echo "  -r, --release       cmake release mode"
    echo "  -m, --make          execute make"
    echo "  -p, --pack          pack to compressed package [7z gz bz2 xz zip]"
    echo "  -P, --platform      build platform (default \"linux/amd64\")"
    echo "  -u, --upx           use upx to compress executable programs"
    echo "  -h, --help          help for $Command"
}


ARGS=`getopt -o hcrmp:P:u --long help,clear,release,make,pack:,platform:,upx -n "$Command" -- "$@"`
eval set -- "${ARGS}"
clear=0
mode="Debug"
make=0
os=linux
arch=amd64
pack=""
upx=0
while true
do
    case "$1" in
        -h|--help)
            help
            exit 0
        ;;
        -c|--clear)
            clear=1
            shift
        ;;
        -u|--upx)
            upx=1
            shift
        ;;
        -r|--release)
            mode="Release"
            shift
        ;;
        -m|--make)
            make=1
            shift
        ;;
        -P|--platform)
            os=${2%\/*}
            arch=${2#*\/}
            if [[ "$os" == "" ]];then
                echo Error: unknown os "$2" for "$Command"
                exit 1
            fi
            if [[ "$arch" == "" ]];then
                echo Error: unknown arch "$2" for "$Command"
                exit 1
            fi
            shift 2
        ;;
        -p|--pack)
            pack="$2"
            shift 2
        ;;
        --)
            shift
            break
        ;;
        *)
            echo Error: unknown flag "$1" for "$Command"
            echo "Run '$Command --help' for usage."
            exit 1
        ;;
    esac
done

cd "$Dir"
# version
"$BashDir/version.sh"

if [[ -d bin/build ]];then
    if [[ $clear == 1 ]];then
        rm bin/build/* -rf
    fi
else
    mkdir bin/build -p
fi
cd bin/build

args=(
    cmake
    -DCMAKE_BUILD_TYPE=$mode
    -DTARGET_OS=$os
    -DTARGET_ARCH=$arch
    ../../
)

# cmake
echo "build for \"$os/$arch\""
exec="${args[@]}"
echo $exec
eval "$exec"

#make
if [[ $make == 0 ]];then
    exit 0
fi
exec="make"
echo $exec
eval "$exec"

cd "$Dir"
# upx 
if [[ $upx == 1 ]];then
    upx "bin/$Target"
fi
# pack
if [[ "$pack" == "" ]];then
    exit 0
fi
"$BashDir/pack_platform.sh" -p "$pack" -P "$os/$arch"