cmake_minimum_required(VERSION 3.0)

set(target_definitions -DUNICODE)
set(target_libs)
if(TARGET_OS STREQUAL linux)
    list(APPEND target_libs
        -static
        stdc++
    )
    if(TARGET_ARCH STREQUAL amd64)
        set(CMAKE_C_COMPILER "gcc")
        set(CMAKE_CXX_COMPILER "g++")    
    else()
        message(FATAL_ERROR "not supported platform: ${TARGET_OS}/${TARGET_ARCH}")
    endif()
elseif(TARGET_OS STREQUAL windows)
    SET(CMAKE_SYSTEM_NAME Windows)
    list(APPEND target_definitions 
        -DWIN32
        -DWIN32_LEAN_AND_MEAN
        -D_WIN32_WINNT=0x0601
    )
    list(APPEND target_libs
        -static
        stdc++
        #-lws2_32
        #-lwsock32
    )
    if(TARGET_ARCH STREQUAL amd64)
        set(CMAKE_C_COMPILER "x86_64-w64-mingw32-gcc-posix")
        set(CMAKE_CXX_COMPILER "x86_64-w64-mingw32-g++-posix")
    else()
        message(FATAL_ERROR "not supported platform: ${TARGET_OS}/${TARGET_ARCH}")
    endif()
else()
    message(FATAL_ERROR "not supported platform: ${TARGET_OS}/${TARGET_ARCH}")
endif()

# 設置 cmake 模塊位置
set(CMAKE_MODULE_PATH 
    "${CMAKE_SOURCE_DIR}/cmake/modules"
)
add_compile_options(-std=gnu++17)

# 添加額外查找路徑 (第三方庫安裝位置)
set(CMAKE_INCLUDE_PATH 
    /opt/lib/${TARGET_OS}_${TARGET_ARCH}/include
)
set(CMAKE_LIBRARY_PATH 
    /opt/lib/${TARGET_OS}_${TARGET_ARCH}/lib
)

# 輔助函數 查找編譯目標
function (push_to_source addtest)
    set(i 2)
    while(i LESS ${ARGC})
         list(GET ARGV ${i} v)
         file(GLOB files 
            "${CMAKE_SOURCE_DIR}/src/${v}" 
        )
        list(LENGTH files l)
        set(j 0)
        while(j LESS ${l})
            list(GET files ${j} file)
            set(added false)
            _push_to_source(added ${addtest} ${file})
            if(added)
                list(APPEND source ${file})
            endif()
            math(EXPR j "${j} + 1")
        endwhile()
         math(EXPR i "${i} + 1")
    endwhile()
    set(source ${source} PARENT_SCOPE)
endfunction()

function(_push_to_source added addtest file)
    get_filename_component(name ${file}  NAME_WLE)
    if(${name} MATCHES "_test$")
        if(NOT addtest)
            return()
        endif()
        string(LENGTH ${name} length)
        math(EXPR length "${length} - 5")
        string(SUBSTRING ${name} 0 ${length} name)
    endif()

    if(${name} MATCHES "^cross")
        if(${name} MATCHES "${TARGET_OS}$")
        elseif(${name} MATCHES "${TARGET_OS}_${TARGET_ARCH}$")
        else()
            return()
        endif()    
    endif()
    set(added true PARENT_SCOPE)
endfunction()


# 設置項目 include 目錄
include_directories("${CMAKE_SOURCE_DIR}/include")

# 修改 目標 輸出目錄
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin") # 可執行檔
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin") # 動態庫
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin") # 靜態庫

project(main)
# 子項目
add_subdirectory(cmake/main)
add_subdirectory(cmake/main_test)