#ifndef __CONTEXT__CONTAINER_HEAP_HPP__
#define __CONTEXT__CONTAINER_HEAP_HPP__
#include <vector>
#include <functional>
namespace king011::container
{
    template <typename Type, typename Compare = std::less<Type>>
    class heap_t final
    {
    private:
        Compare less_;

    public:
        typedef Type value_type;
        std::vector<value_type> array;

        explicit heap_t() = default;
        ~heap_t() = default;
        heap_t(const heap_t &o) = delete;
        heap_t &operator=(const heap_t &o) = delete;
        explicit heap_t(heap_t &&o) : less_()
        {
            this->array = std::move(o.array);
        }
        explicit heap_t(std::vector<value_type> &&arr) : less_()
        {
            array = std::move(arr);
        }
        heap_t &operator=(heap_t &&o)
        {
            array = std::move(o.array);
            return *this;
        }
        heap_t &operator=(std::vector<value_type> &&arr)
        {
            array = std::move(arr);
            return *this;
        }
        inline void swap(heap_t &o)
        {
            array.swap(o.array);
        }

        // 初始化堆
        void init()
        {
            // heapify
            int n = array.size();
            for (int i = n / 2 - 1; i >= 0; i--)
            {
                _down(i, n);
            }
        }
        // 返回堆頂元素
        inline value_type &top() noexcept
        {
            return array.front();
        }
        inline const value_type &top() const noexcept
        {
            return array.front();
        }
        // 將堆頂元素彈出
        void pop()
        {
            int n = array.size();
            --n;
            _swap(0, n);
            _down(0, n);

            array.pop_back();
        }
        // 向堆中添加元素
        inline void push(value_type &v)
        {
            array.push_back(v);
            _up(array.size() - 1);
        }
        // 向堆中添加元素
        inline void push(value_type &&v)
        {
            array.push_back(std::move(v));
            _up(array.size() - 1);
        }
        // 當堆中元素變化 修復堆性質
        inline void fix(int i)
        {
            if (!_down(i, array.size()))
            {
                _up(i);
            }
        }
        // 移除堆中元素
        void remove(int i)
        {
            int n = array.size();
            --n;
            if (n != i)
            {
                _swap(i, n);
                if (!_down(i, n))
                {
                    _up(i);
                }
            }
            array.pop_back();
        }

        inline bool empty() const noexcept
        {
            return array.empty();
        }
        inline std::size_t size() const noexcept
        {
            return array.size();
        }
        inline value_type &operator[](std::size_t i) noexcept
        {
            return array[i];
        }
        inline const value_type &operator[](std::size_t i) const noexcept
        {
            return array[i];
        }

    private:
        inline bool _less(int i, int j)
        {
            return less_(array[i], array[j]);
        }
        inline void _swap(int i, int j)
        {
            return std::swap(array[i], array[j]);
        }
        bool _down(int i0, int n)
        {
            auto i = i0;
            for (;;)
            {
                auto j1 = 2 * i + 1;
                if (j1 >= n || j1 < 0)
                { // j1 < 0 after int overflow
                    break;
                }
                auto j = j1; // left child
                if (auto j2 = j1 + 1; j2 < n && _less(j2, j1))
                {
                    j = j2; // = 2*i + 2  // right child
                }
                if (!_less(j, i))
                {
                    break;
                }
                _swap(i, j);
                i = j;
            }
            return i > i0;
        }
        void _up(int j)
        {
            for (;;)
            {
                auto i = (j - 1) / 2; // parent
                if (i == j || !_less(j, i))
                {
                    break;
                }
                _swap(i, j);
                j = i;
            }
        }
    };
};
#endif // __CONTEXT__CONTAINER_HEAP_HPP__