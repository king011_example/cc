#ifndef __CONTEXT__CONTEXT_SHARED_WORK_HPP__
#define __CONTEXT__CONTEXT_SHARED_WORK_HPP__
#include "basic.hpp"
#include <thread>
#include <container/heap.hpp>
namespace king011::context
{
    class priority_type
    {
    public:
        std::size_t id;
        std::size_t index;
        std::size_t work;
        priority_type(std::size_t id = 0, std::size_t index = 0, std::size_t work = 0)
            : id(id),
              index(index),
              work(work) {}
    };
    class priority_wrapper
    {
    public:
        priority_type *impl;
        priority_wrapper(priority_type *impl = nullptr)
            : impl(impl) {}

        // 重載比較
        bool operator<(const priority_wrapper &o) const noexcept
        {
            return impl->work < o.impl->work;
        }
        void swap(priority_wrapper &o) noexcept
        {
            std::swap(impl, o.impl);
            std::swap(impl->index, o.impl->index);
        }
    };

    template <typename Mutex = std::mutex>
    class shared_work_context_t final : public basic_context_t
    {
    public:
        typedef Mutex mutex_type;

    private:
        typedef std::promise<boost::system::error_code> promise_type;
        typedef std::shared_future<boost::system::error_code> future_type;
        typedef std::pair<std::shared_ptr<promise_type>, future_type> completer_type;
        // 執行上下文
        std::vector<context_type> contexts_;
        // 防止 io_context 空閒退出
        std::vector<context_type::work> works_;
        completer_type completer_;
        // 同步對象
        mutex_type mutex_;
        // 隊列索引
        std::vector<priority_type> keys_;
        // 優先隊列
        king011::container::heap_t<priority_wrapper> priority_;
        // 儘量避免 隊列排序的 調整值
        std::size_t adjust_;

    public:
        explicit shared_work_context_t(std::size_t concurrency = std::thread::hardware_concurrency(), std::size_t adjust = 50)
            : adjust_(adjust)
        {
            if (concurrency < 1)
            {
                throw std::length_error("concurrency should be > 0");
            }
            contexts_ = std::move(std::vector<context_type>(concurrency));
            for (std::size_t i = 0; i < concurrency; i++)
            {
                keys_.emplace_back(i, i);
            }
            auto data = keys_.data();
            for (size_t i = 0; i < concurrency; i++)
            {
                priority_.array.emplace_back(data + i);
            }
        }
        shared_work_context_t(const shared_work_context_t &) = delete;
        shared_work_context_t *operator=(const shared_work_context_t &) = delete;
        virtual ~shared_work_context_t()
        {
            stop();
        }
        virtual pair_type get_context() override
        {
            std::lock_guard<mutex_type> lock(mutex_);
            auto elem = priority_.top().impl;
            auto id = elem->id;
            elem->work++;
            if (adjust_ < 2 || elem->work % adjust_ == 0)
            {
                auto index = elem->index;
                priority_.fix(index);
            }
            return pair_type(id, contexts_[id]);
        }
        virtual std::vector<pair_type> get_contexts() override
        {
            std::lock_guard<mutex_type> lock(mutex_);
            std::vector<pair_type> result;
            for (auto &node : keys_)
            {
                node.work++;
                result.emplace_back(node.id, contexts_[node.id]);
            }
            if (adjust_ > 2)
            {
                priority_.init();
            }
            return std::move(result);
        }
        virtual void put_context(std::size_t id) override
        {
            std::lock_guard<mutex_type> lock(mutex_);
            auto &elem = keys_[id];
#ifdef __CONTEXT__CONTEXT_SHARED_WORK_TEST__
            if (elem.id != id)
            {
                throw std::runtime_error("put_context error: id not match");
            }
            auto index = elem.index;
            auto impl = (priority_[index]).impl;
            if (impl->index != index)
            {
                throw std::runtime_error("put_context error: index not match");
            }
            else if (impl->id != id)
            {
                throw std::runtime_error("put_context error: id2 not match");
            }
#endif // __CONTEXT__CONTEXT_SHARED_WORK_TEST__
            if (elem.work > 0)
            {
                elem.work--;
                if (adjust_ < 2 || elem.work % adjust_ == 0)
                {
                    auto index = elem.index;
                    priority_.fix(index);
                }
            }
            else
            {
                throw std::runtime_error("put_context error: elem's work is 0");
            }
        }
        virtual void run() override
        {
            completer_type completer;
            {
                std::lock_guard<mutex_type> lock(mutex_);
                if (completer_.first)
                {
                    completer = completer_;
                }
                else
                {
                    auto promise = std::make_shared<promise_type>();
                    completer = std::make_pair(promise, promise->get_future().share());
                    completer_ = completer;
                    auto count = contexts_.size();
                    for (size_t i = 0; i < count; i++)
                    {
                        works_.emplace_back(contexts_[i]);
                    }
                    std::thread([
#ifdef __CONTEXT__CONTEXT_SHARED_WORK_TRACE__
                                    self = this,
#endif //__CONTEXT__CONTEXT_SHARED_WORK_TRACE__
                                    promise, &contexts = contexts_]
                                {
                                    std::vector<std::thread> threads;
                                    auto count = contexts.size();
                                    for (size_t i = 1; i < count; i++)
                                    {
                                        threads.emplace_back([&context = contexts[i]]
                                                             {
                                                                 boost::system::error_code ec;
                                                                 context.run(ec);
                                                             });
                                    }
#ifdef __CONTEXT__CONTEXT_SHARED_WORK_TRACE__
                                    std::atomic_bool trace = true;
                                    std::thread thread([&trace, &keys = self->keys_, &mutex = self->mutex_]
                                                       {
                                                           while (trace)
                                                           {
                                                               std::this_thread::sleep_for(std::chrono::seconds(1));
                                                               mutex.lock();
                                                               for (auto &node : keys)
                                                               {
                                                                   std::cout << "id=" << node.id << " index=" << node.index << " work=" << node.work << "\n";
                                                               }
                                                               std::cout << std::endl;
                                                               mutex.unlock();
                                                           }
                                                       });
#endif // __CONTEXT__CONTEXT_SHARED_WORK_TRACE__

                                    boost::system::error_code ec;
                                    contexts[0].run(ec);
                                    for (auto &thread : threads)
                                    {
                                        thread.join();
                                    }
#ifdef __CONTEXT__CONTEXT_SHARED_WORK_TRACE__
                                    trace = false;
                                    thread.join();
#endif // __CONTEXT__CONTEXT_SHARED_WORK_TRACE__
                                    promise->set_value(ec);
                                })
                        .detach();
                }
            }
            // 等待完成
            auto ec = completer.second.get();
            {
                std::lock_guard<mutex_type> lock(mutex_);
                if (completer.first == completer_.first)
                {
                    completer_.first.reset();
                }
            }
            if (ec)
            {
                throw boost::system::system_error(ec);
            }
        }
        virtual void stop() override
        {
            std::lock_guard<mutex_type> lock(mutex_);
            for (auto &context : contexts_)
            {
                context.stop();
            }
            works_.clear();
        }
    };
};
namespace std
{
    // 特化 swap
    template <>
    void swap<king011::context::priority_wrapper>(king011::context::priority_wrapper &a, king011::context::priority_wrapper &b)
    {
        a.swap(b);
    }
};
#endif // __CONTEXT__CONTEXT_SHARED_WORK_HPP__