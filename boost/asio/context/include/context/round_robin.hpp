#ifndef __CONTEXT__CONTEXT_ROUND_ROBIN_HPP__
#define __CONTEXT__CONTEXT_ROUND_ROBIN_HPP__
#include "basic.hpp"
#include <thread>
namespace king011::context
{
    template <typename Mutex = std::mutex>
    class round_robin_context_t final : public basic_context_t
    {
    public:
        typedef Mutex mutex_type;

    private:
        typedef std::promise<boost::system::error_code> promise_type;
        typedef std::shared_future<boost::system::error_code> future_type;
        typedef std::pair<std::shared_ptr<promise_type>, future_type> completer_type;
        // 執行上下文
        std::vector<context_type> contexts_;
        // 防止 io_context 空閒退出
        std::vector<context_type::work> works_;
        completer_type completer_;
        // 同步對象
        mutex_type mutex_;
        // 循環 索引
        std::size_t next_;

    public:
        explicit round_robin_context_t(std::size_t concurrency = std::thread::hardware_concurrency()) : next_(0)
        {
            if (concurrency < 1)
            {
                throw std::length_error("concurrency should be > 0");
            }
            contexts_ = std::move(std::vector<context_type>(concurrency));
        }
        round_robin_context_t(const round_robin_context_t &) = delete;
        round_robin_context_t *operator=(const round_robin_context_t &) = delete;
        virtual ~round_robin_context_t()
        {
            stop();
        }
        // 返回下個 context 並移動索引
        virtual pair_type get_context() override
        {
            std::lock_guard<mutex_type> lock(mutex_);
            auto i = next_++;
            if (next_ == contexts_.size())
            {
                next_ = 0;
            }
            return pair_type(i, contexts_[i]);
        }
        virtual std::vector<pair_type> get_contexts() override
        {
            std::vector<pair_type> result;
            std::size_t i;
            for (auto &context : contexts_)
            {
                result.emplace_back(i++, context);
            }
            return std::move(result);
        }
        virtual void put_context(std::size_t) override {}
        virtual void run() override
        {
            completer_type completer;
            {
                std::lock_guard<mutex_type> lock(mutex_);
                if (completer_.first)
                {
                    completer = completer_;
                }
                else
                {
                    auto promise = std::make_shared<promise_type>();
                    completer = std::make_pair(promise, promise->get_future().share());
                    completer_ = completer;
                    auto count = contexts_.size();
                    for (size_t i = 0; i < count; i++)
                    {
                        works_.emplace_back(contexts_[i]);
                    }
                    std::thread([promise, &contexts = contexts_]
                                {
                                    std::vector<std::thread> threads;
                                    auto count = contexts.size();
                                    for (size_t i = 1; i < count; i++)
                                    {
                                        threads.emplace_back([&context = contexts[i]]
                                                             {
                                                                 boost::system::error_code ec;
                                                                 context.run(ec);
                                                             });
                                    }
                                    boost::system::error_code ec;
                                    contexts[0].run(ec);
                                    for (auto &thread : threads)
                                    {
                                        thread.join();
                                    }
                                    promise->set_value(ec);
                                })
                        .detach();
                }
            }
            // 等待完成
            auto ec = completer.second.get();
            {
                std::lock_guard<mutex_type> lock(mutex_);
                if (completer.first == completer_.first)
                {
                    completer_.first.reset();
                }
            }
            if (ec)
            {
                throw boost::system::system_error(ec);
            }
        }
        virtual void stop() override
        {
            std::lock_guard<mutex_type> lock(mutex_);
            for (auto &context : contexts_)
            {
                context.stop();
            }
            works_.clear();
        }
    };
};
#endif // __CONTEXT__CONTEXT_ROUND_ROBIN_HPP__