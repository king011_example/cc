#ifndef __CONTEXT__CONTEXT_BASIC_HPP__
#define __CONTEXT__CONTEXT_BASIC_HPP__
#include <boost/asio.hpp>
namespace king011::context
{
    class basic_context_t
    {
    public:
        typedef boost::asio::io_context context_type;
        typedef std::pair<std::size_t, context_type &> pair_type;

        basic_context_t() = default;
        virtual ~basic_context_t() = default;
        // 獲取一個就緒的 context 和其索引 並爲其增加任務計數
        virtual pair_type get_context() = 0;
        // 返回所有獨立的 context 並爲其增加任務計數
        virtual std::vector<pair_type> get_contexts() = 0;
        // 減少 context 上的任務計數
        virtual void put_context(std::size_t) = 0;
        // 運行 context
        virtual void run() = 0;
        // 停止 context
        virtual void stop() = 0;
    };
    class context_guard_t final
    {
    public:
        typedef std::pair<std::size_t, basic_context_t::context_type &> value_type;

    private:
        value_type value_;
        basic_context_t &context_;

    public:
        explicit context_guard_t(basic_context_t &context)
            : context_(context),
              value_(context.get_context())
        {
        }
        explicit context_guard_t(const context_guard_t &) = delete;
        context_guard_t &operator=(const context_guard_t &) = delete;
        ~context_guard_t()
        {
            context_.put_context(value_.first);
        }
        basic_context_t::context_type &get_context() noexcept
        {
            return value_.second;
        }
    };
    class unique_context_t final
    {
    public:
    private:
        std::size_t id_ = 0;
        basic_context_t::context_type *type_ = nullptr;
        basic_context_t *context_ = nullptr;

    public:
        explicit unique_context_t(basic_context_t &context)
            : context_(&context)
        {
            auto elem = context.get_context();
            id_ = elem.first;
            type_ = &elem.second;
        }
        unique_context_t(unique_context_t &&o) noexcept : context_(o.context_)
        {
            id_ = o.id_;
            type_ = o.type_;

            o.context_ = nullptr;
        }
        explicit unique_context_t(const unique_context_t &) = delete;
        unique_context_t &operator=(const unique_context_t &) = delete;
        unique_context_t &operator=(unique_context_t &&o) noexcept
        {
            if (context_)
            {
                context_->put_context(id_);
                context_ = nullptr;
            }

            context_ = o.context_;
            id_ = o.id_;
            type_ = o.type_;
            if (o.context_)
            {
                o.context_ = nullptr;
            }
            return *this;
        }
        ~unique_context_t()
        {
            put_context();
        }
        void put_context()
        {
            if (context_)
            {
                context_->put_context(id_);
                context_ = nullptr;
            }
        }
        basic_context_t::context_type &get_context() noexcept
        {
            return *type_;
        }
        operator bool() const noexcept
        {
            return context_;
        }
    };
}
#endif // __CONTEXT__CONTEXT_BASIC_HPP__