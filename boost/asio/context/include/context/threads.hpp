#ifndef __CONTEXT__CONTEXT_THREAD_HPP__
#define __CONTEXT__CONTEXT_THREAD_HPP__
#include "basic.hpp"
#include <thread>
namespace king011::context
{
    template <typename Mutex = std::mutex>
    class threads_context_t final : public basic_context_t
    {
    public:
        typedef Mutex mutex_type;

    private:
        typedef std::promise<boost::system::error_code> promise_type;
        typedef std::shared_future<boost::system::error_code> future_type;
        typedef std::pair<std::shared_ptr<promise_type>, future_type> completer_type;
        // 執行上下文
        context_type context_;
        // 防止 io_context 空閒退出
        std::vector<context_type::work> works_;
        completer_type completer_;
        // 同步對象
        mutex_type mutex_;
        // 併發線程數
        std::size_t concurrency_;

    public:
        explicit threads_context_t(std::size_t concurrency = std::thread::hardware_concurrency())
        {
            if (concurrency < 1)
            {
                throw std::length_error("concurrency should be > 0");
            }
            concurrency_ = concurrency;
        }
        threads_context_t(const threads_context_t &) = delete;
        threads_context_t *operator=(const threads_context_t &) = delete;
        virtual ~threads_context_t()
        {
            stop();
        }
        virtual pair_type get_context() override
        {
            return pair_type(0, context_);
        }
        virtual std::vector<pair_type> get_contexts() override
        {
            return std::vector<pair_type>(1, pair_type(0, context_));
        }
        virtual void put_context(std::size_t) override {}
        virtual void run() override
        {
            completer_type completer;
            {
                std::lock_guard<mutex_type> lock(mutex_);
                if (completer_.first)
                {
                    completer = completer_;
                }
                else
                {
                    auto promise = std::make_shared<promise_type>();
                    completer = std::make_pair(promise, promise->get_future().share());
                    completer_ = completer;
                    works_.emplace_back(context_);
                    std::thread([promise, &context = context_, concurrency = concurrency_]
                                {
                                    std::vector<std::thread> threads;
                                    for (size_t i = 1; i < concurrency; i++)
                                    {
                                        threads.emplace_back([&context]
                                                             {
                                                                 boost::system::error_code ec;
                                                                 context.run(ec);
                                                             });
                                    }
                                    boost::system::error_code ec;
                                    context.run(ec);
                                    for (auto &thread : threads)
                                    {
                                        thread.join();
                                    }
                                    promise->set_value(ec);
                                })
                        .detach();
                }
            }
            // 等待完成
            auto ec = completer.second.get();
            {
                std::lock_guard<mutex_type> lock(mutex_);
                if (completer.first == completer_.first)
                {
                    completer_.first.reset();
                }
            }
            if (ec)
            {
                throw boost::system::system_error(ec);
            }
        }
        virtual void stop() override
        {
            std::lock_guard<mutex_type> lock(mutex_);
            context_.stop();
            works_.clear();
        }
    };
}
#endif // __CONTEXT__CONTEXT_THREAD_HPP__