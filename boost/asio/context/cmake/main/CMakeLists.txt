set(target_name context)
project(${target_name})

#查找源碼
set(source)
push_to_source(
    source
    false
    main.cc
    # context/*.cc
)

# 查找依賴

find_package(Boost
    1.76.0
    REQUIRED
    COMPONENTS 
        coroutine
        thread
)
list(APPEND target_headers ${Boost_INCLUDE_DIRS})
list(APPEND target_libs ${Boost_LIBRARIES})


# 刪除 重複項
if(source)
    list(REMOVE_DUPLICATES source)
endif()
if(target_headers)
    list(REMOVE_DUPLICATES target_headers)
endif()
if(target_libs)
    list(REMOVE_DUPLICATES target_libs)
endif()

add_definitions(${target_definitions})
include_directories(${target_headers})
add_executable(${target_name} ${source})
target_link_libraries(${target_name} ${target_libs})