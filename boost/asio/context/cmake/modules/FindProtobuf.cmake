include(FindPackageHandleStandardArgs)

set(Protobuf_FOUND FALSE)
set(Protobuf_INCLUDE_DIRS)
set(Protobuf_LIBRARIES)

# 查找 頭文件
find_path(Protobuf_INCLUDE_DIRS NAMES google/protobuf/service.h)
if(NOT Protobuf_INCLUDE_DIRS)
    if(Protobuf_FIND_REQUIRED)
        message(FATAL_ERROR "Protobuf : Could not find google/protobuf/service.h")
    else()
        message(WARNING "Protobuf : Could not find google/protobuf/service.h")
    endif()
    return()
endif()


# 查找 庫文件
find_library(Protobuf_LIBRARIE_Protobuf NAMES protobuf)
if(NOT Protobuf_LIBRARIE_Protobuf)
    if(Protobuf_FIND_REQUIRED)
        message(FATAL_ERROR "Protobuf : Could not find lib protobuf")
    else()
        message(WARNING "Protobuf : Could not find lib protobuf")
    endif()
    return()
endif()
find_library(Protobuf_LIBRARIE_ProtobufPP NAMES protobuf-lite)
if(NOT Protobuf_LIBRARIE_ProtobufPP)
    if(Protobuf_FIND_REQUIRED)
        message(FATAL_ERROR "Protobuf : Could not find lib protobuf-lite")
    else()
        message(WARNING "Protobuf : Could not find lib protobuf-lite")
    endif()
    return()
endif()
find_library(Protobuf_LIBRARIE_GRP NAMES protoc)
if(NOT Protobuf_LIBRARIE_GRP)
    if(Protobuf_FIND_REQUIRED)
        message(FATAL_ERROR "Protobuf : Could not find lib protoc")
    else()
        message(WARNING "Protobuf : Could not find lib protoc")
    endif()
    return()
endif()
list(APPEND Protobuf_LIBRARIES
    "${Protobuf_LIBRARIE_Protobuf}"
    "${Protobuf_LIBRARIE_ProtobufPP}"
    "${Protobuf_LIBRARIE_GRP}"
)
list(REMOVE_DUPLICATES Protobuf_INCLUDE_DIRS)
list(REMOVE_DUPLICATES Protobuf_LIBRARIES)

message(STATUS "Found Protobuf: ${Protobuf_LIBRARIES}")

# 設置 庫 信息
find_package_handle_standard_args(Protobuf
		DEFAULT_MSG
		Protobuf_INCLUDE_DIRS
        Protobuf_LIBRARIES
)
