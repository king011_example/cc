#include <iostream>
#include "CLI11.hpp"
#include "version.h"
// #define __CONTEXT__CONTEXT_SHARED_WORK_TEST__
// #define __CONTEXT__CONTEXT_SHARED_WORK_TRACE__
#include "context/round_robin.hpp"
#include "context/threads.hpp"
#include "context/shared_work.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#define BUFFER_SIZE (1024 * 32)
typedef boost::asio::io_context io_context_t;
typedef boost::asio::ip::tcp::acceptor acceptor_t;
typedef boost::asio::ip::tcp::socket socket_t;
typedef boost::asio::ip::tcp::endpoint endpoint_t;
endpoint_t get_endpoint(const std::string &addr)
{
    auto find = addr.find_last_of(":");
    if (find == std::string::npos)
    {
        throw std::domain_error("bad addr");
    }
    auto port = boost::lexical_cast<boost::asio::ip::port_type>(addr.substr(find + 1));
    auto domain = addr.substr(0, find);
    if (domain.empty())
    {
        return endpoint_t(boost::asio::ip::tcp::v6(), port);
    }
    else
    {
        return endpoint_t(boost::asio::ip::address::from_string(domain), port);
    }
}
void accept_sleep(const std::exception &e, std::size_t &temp_delay, acceptor_t &acceptor, boost::asio::yield_context yield)
{
    if (temp_delay)
    {
        temp_delay *= 2;
        if (temp_delay > 1000)
        {
            temp_delay = 1000;
        }
    }
    else
    {
        temp_delay = 5;
    }

    boost::asio::deadline_timer sleep(acceptor.get_executor(), boost::posix_time::milliseconds(temp_delay));
    std::cerr << "Accept error: " << e.what()
              << "; retrying in " << temp_delay << "ms"
              << std::endl;

    sleep.async_wait(yield);
}
void accept_routine(king011::context::basic_context_t &pool, acceptor_t &acceptor, boost::asio::yield_context yield)
{
    std::size_t temp_delay = 0;
    for (;;)
    {
        try
        {
            // 接受一個新連接
            king011::context::unique_context_t unique_context(pool);
            auto &context = unique_context.get_context();
            socket_t s(context);
            acceptor.async_accept(s, yield);
            temp_delay = 0;
            // 啓動 socket 協程
            boost::asio::spawn(context, [s = boost::move(s), unique_context = std::move(unique_context)](boost::asio::yield_context yield) mutable
                               {
                                   auto remote = s.remote_endpoint();
                                   try
                                   {
                                       char buffer[BUFFER_SIZE];
                                       for (;;)
                                       {
                                           // 讀取
                                           size_t length = s.async_read_some(
                                               boost::asio::buffer(buffer, BUFFER_SIZE),
                                               yield);
                                           // 寫入
                                           s.async_write_some(
                                               boost::asio::buffer(buffer, length),
                                               yield);
                                       }
                                   }
                                   catch (const boost::system::system_error &e)
                                   {
                                       if (e.code().value() != boost::asio::error::eof)
                                       {
                                           std::cout << boost::diagnostic_information(e) << std::endl;
                                       }
                                   }
                                   catch (const std::exception &e)
                                   {
                                       std::cout << e.what() << std::endl;
                                   }
                               });
        }
        catch (const std::exception &e)
        {
            accept_sleep(e, temp_delay, acceptor, yield);
        }
    }
}
void run(const std::string &mode, king011::context::basic_context_t &pool, const std::string &addr)
{
    // 創建監聽器
    auto endpoint = get_endpoint(addr);
    auto context = pool.get_context();
    acceptor_t acceptor(context.second, endpoint);
    std::cout << mode << " work at " << addr << std::endl;

    auto contexts = pool.get_contexts();
    for (auto &context : contexts)
    {
        // 運行監聽器協程
        boost::asio::spawn(
            context.second,
            std::bind(accept_routine,
                      std::ref(pool),
                      std::ref(acceptor),
                      std::placeholders::_1));
    }

    // 運行服務
    pool.run();
}
int main(int argc, char *argv[])
{
    CLI::App app("context");
    app.callback(
        [&cmd = app]
        {
            const CLI::Option &v = *cmd.get_option("--version");
            auto addr = cmd.get_option("--addr")->as<std::string>();
            if (v)
            {
                std::cout << CONTEXT_VERSION
                          << std::endl;
            }
            else if (!addr.empty())
            {
                auto mode = cmd.get_option("--mode")->as<std::string>();
                if ("threads" == mode)
                {
                    king011::context::threads_context_t context;
                    run(mode, context, addr);
                }
                else if ("round" == mode)
                {
                    king011::context::round_robin_context_t context;
                    run(mode, context, addr);
                }
                else if ("shared" == mode)
                {
                    king011::context::shared_work_context_t context;
                    run(mode, context, addr);
                }
                else
                {
                    throw std::runtime_error("unknow mode");
                }
            }
            else
            {
                throw CLI::CallForHelp();
            }
        });
    app.add_flag("-v,--version", "display version");
    std::string addr = ":9000";
    app.add_option("-a,--addr",
                   addr,
                   "listen addr",
                   true);
    std::string mode = "shared";
    app.add_option("-m,--mode",
                   mode,
                   "work mode [threads round shared]",
                   true);
    CLI11_PARSE(app, argc, argv);
    return 0;
}