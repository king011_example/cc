#include <string>
#include <map>
#include <gtest/gtest.h>
#include "container/heap.hpp"
typedef king011::container::heap_t<int> heap_t;
void verify(heap_t &h, int i)
{
    int n = h.size();
    int j1 = 2 * i + 1;
    int j2 = 2 * i + 2;
    if (j1 < n)
    {
        if (h[j1] < h[i])
        {
            std::ostringstream o;
            o << "heap invariant invalidated [" << i << "] = " << h[i] << " > [" << j1 << "] = " << h[j1];
            GTEST_FATAL_FAILURE_(o.str().c_str());
            return;
        }
        verify(h, j1);
    }
    if (j2 < n)
    {
        if (h[j2] < h[i])
        {
            std::ostringstream o;
            o << "heap invariant invalidated [" << i << "] = " << h[i] << " > [" << j2 << "] = " << h[j2];
            GTEST_FATAL_FAILURE_(o.str().c_str());
            return;
        }
        verify(h, j2);
    }
}
TEST(Init0Test, Container_Heap)
{
    king011::container::heap_t<int> h;
    for (int i = 20; i > 0; i--)
    {
        h.array.push_back(0); // all elements are the same
    }
    h.init();
    verify(h, 0);

    for (int i = 1; !h.empty(); i++)
    {
        auto x = h.top();
        h.pop();
        verify(h, 0);
        EXPECT_EQ(x, 0);
    }
}
TEST(Init1Test, Container_Heap)
{
    king011::container::heap_t<int> h;
    for (int i = 20; i > 0; i--)
    {
        h.array.push_back(i); // all elements are different
    }
    h.init();
    verify(h, 0);

    for (int i = 1; !h.empty(); i++)
    {
        auto x = h.top();
        h.pop();
        verify(h, 0);
        EXPECT_EQ(x, i);
    }
}
TEST(Test, Container_Heap)
{
    king011::container::heap_t<int> h;
    verify(h, 0);

    for (int i = 20; i > 10; i--)
    {
        h.array.push_back(i);
    }
    h.init();
    verify(h, 0);

    for (int i = 10; i > 0; i--)
    {
        h.push(i);
        verify(h, 0);
    }

    for (int i = 1; !h.empty(); i++)
    {
        auto x = h.top();
        h.pop();
        if (i < 20)
        {
            h.push(20 + i);
        }
        verify(h, 0);
        EXPECT_EQ(x, i);
    }
}
TEST(Remove0Test, Container_Heap)
{
    king011::container::heap_t<int> h;
    for (int i = 0; i < 10; i++)
    {
        h.array.push_back(i);
    }
    verify(h, 0);

    while (!h.empty())
    {
        int i = h.size() - 1;
        auto x = h[i];
        h.remove(i);
        EXPECT_EQ(x, i);
        verify(h, 0);
    }
}
TEST(Remove1Test, Container_Heap)
{
    king011::container::heap_t<int> h;
    for (int i = 0; i < 10; i++)
    {
        h.array.push_back(i);
    }
    verify(h, 0);

    for (int i = 0; !h.empty(); i++)
    {
        int x = h[0];
        h.remove(0);
        EXPECT_EQ(x, i);
        verify(h, 0);
    }
}
TEST(Remove2Test, Container_Heap)
{
    int N = 10;
    king011::container::heap_t<int> h;
    for (int i = 0; i < N; i++)
    {
        h.array.push_back(i);
    }
    verify(h, 0);

    std::map<int, bool> m;
    while (!h.empty())
    {
        int i = (h.size() - 1) / 2;
        auto v = h[i];
        m[v] = true;
        h.remove(i);
        verify(h, 0);
    }

    EXPECT_EQ(N, m.size());
    for (int i = 0; i < m.size(); i++)
    {
        EXPECT_TRUE(m[i]);
    }
}

TEST(FixTest, Container_Heap)
{
    king011::container::heap_t<int> h;
    verify(h, 0);
    for (int i = 200; i > 0; i -= 10)
    {
        h.push(i);
    }
    verify(h, 0);

    EXPECT_EQ(h[0], 10);
    h[0] = 210;
    h.fix(0);
    verify(h, 0);
    std::srand(std::time(nullptr));
    for (int i = 100; i > 0; i--)
    {
        int elem = std::rand() % h.size();
        if (i & 1 == 0)
        {
            h[elem] *= 2;
        }
        else
        {
            h[elem] /= 2;
        }
        h.fix(elem);
        verify(h, 0);
    }
}